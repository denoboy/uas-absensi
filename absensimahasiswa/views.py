from .models import Mahasiswa
from django.shortcuts import render
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.
var = {
    'judul' : 'Sistem Informasi Absensi Mahasiswa',
    'info' : '''Sistem Informasi untuk absensi''',
    'oleh' : 'owner'         
}
def index(self):
    var['mahasiswa'] = Mahasiswa.objects.values('id','nama_mahasiswa','semester', 'matkul').\
        order_by('nama_mahasiswa')
    return render(self, 'absensi/index.html',context=var)

class MahasiswaDetailView(DetailView):
    model = Mahasiswa
    template_name = 'absensi/mahasiswa_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class MahasiswaCreateView(CreateView):
    model = Mahasiswa
    fields = '__all__'
    template_name = 'absensi/mahasiswa_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class MahasiswaEditView(UpdateView):
    model = Mahasiswa
    fields = ['nama_mahasiswa','nim','semester','matkul','keterangan']
    template_name = 'absensi/mahasiswa_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class MahasiswaDeleteView(DeleteView):
    model = Mahasiswa
    template_name = 'absensi/mahasiswa_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context