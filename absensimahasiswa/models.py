from django.db import models
from django.urls import reverse
from django.utils import timezone

# Create your models here.

class Mahasiswa(models.Model):
    MAHASISWA_CHOICES = (
        ('1','Semester Satu'),
        ('2','Semester Dua'),
        ('3','Semester Tiga'),
        ('4','Semester Empat'),
        ('5','Semester Lima'),
        ('6','Semester Enam'),
    )
    nama_mahasiswa = models.CharField('Nama Mahasiswa', max_length=50, null=False)
    nim = models.CharField('NIM', max_length=50, null=False)
    semester = models.CharField(max_length=2, choices=MAHASISWA_CHOICES)
    matkul = models.TextField()
    keterangan = models.TextField()

    def __str__(self):
        return self.nama_mahasiswa

    def get_absolute_url(self):
        return reverse('home_page')