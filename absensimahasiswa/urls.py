from django.urls import path
from .views import index, MahasiswaDetailView, MahasiswaCreateView, MahasiswaEditView, MahasiswaDeleteView

urlpatterns = [
    path('', index, name='home_page'),
    path('mahasiswa/<int:pk>', MahasiswaDetailView.as_view(), name='mahasiswa_detail_view'),
    path('mahasiswa/add', MahasiswaCreateView.as_view(), name='mahasiswa_add'),
    path('mahasiswa/edit/<int:pk>', MahasiswaEditView.as_view(), name='mahasiswa_edit'),
    path('mahasiswa/delete/<int:pk>', MahasiswaDeleteView.as_view(), name='mahasiswa_delete'),
]
